package com.openweather.exam;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.openweather.exam.entity.WeatherSummary;

public class MainActivity extends AppCompatActivity implements WeatherListFragment.OnListFragmentInteractionListener, RefreshFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        RefreshFragment refreshFragment = new RefreshFragment();
        FragmentManager fm2 = getSupportFragmentManager();
        FragmentTransaction ft2 = fm2.beginTransaction();
        ft2.replace(R.id.frameLayoutRefresh, refreshFragment);
        ft2.commit();

        refreshFragment();
    }


    @Override
    public void onListFragmentInteraction(WeatherSummary item) {

        Intent i = new Intent(this, WeatherActivity.class);
        Bundle bundle = new Bundle();


        bundle.putString("COUNTRYID", String.valueOf(item.getCountryID()));
        i.putExtras(bundle);
        startActivity(i);

    }

    @Override
    public void onFragmentInteraction() {
        refreshFragment();
    }

    private void refreshFragment() {
        WeatherListFragment weatherListFragment = new WeatherListFragment();

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frameLayout, weatherListFragment);
        ft.commit();
    }


}
