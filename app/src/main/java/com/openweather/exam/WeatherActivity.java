package com.openweather.exam;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.openweather.exam.responses.Coord;
import com.openweather.exam.responses.Result;
import com.openweather.exam.responses.Weather;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherActivity extends AppCompatActivity {

    String CountryID;
    TextView txtWeather, txtWeatherDesc, txtLoc;
    ImageView imgView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        CountryID = getIntent().getExtras().getString("COUNTRYID");

        txtLoc = findViewById(R.id.txtLocation);
        txtWeather = findViewById(R.id.txtWeather);
        txtWeatherDesc = findViewById(R.id.txtWeatherDesc);
        imgView = findViewById(R.id.imgIcon);

        Button btnRefresh = findViewById(R.id.btnRefresh);

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadWeatherInfo(CountryID);
            }
        });

        loadWeatherInfo(CountryID);

    }

    private void loadWeatherInfo(String CountryID) {
        ApiService apiService = ApiClient.getClient().create(ApiService.class);

        Map<String, String> data = new HashMap<>();
        data.put("appid", "72535be0604fee283e9f63598c7b4836");
        data.put("id", CountryID);

        Call<Result> resultCall = apiService.getWeatherInfo(data);
        resultCall.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                if(response.code() == 200) {
                    String wMain = response.body().getWeather().get(0).getMain();
                    String icon =response.body().getWeather().get(0).getIcon();
                    String loc = response.body().getName();

                    String temp = String.valueOf(response.body().getMain().getTemp());

                    txtLoc.setText(loc);
                    txtWeatherDesc.setText(temp);
                    txtWeather.setText(wMain);
                    Picasso.with(getApplicationContext())
                            .load("https://openweathermap.org/img/w/"+icon+".png")
                            .into(imgView);

                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                Toast.makeText(WeatherActivity.this, t.getCause().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
