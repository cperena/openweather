package com.openweather.exam;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.openweather.exam.entity.WeatherSummary;
import com.openweather.exam.responses.ListWeather;
import com.openweather.exam.responses.Main;
import com.openweather.exam.responses.ResultResp;
import com.openweather.exam.responses.Weather;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class WeatherListFragment extends Fragment {


    private OnListFragmentInteractionListener mListener;



    RecyclerView recyclerView;
    public WeatherListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
//    public static WeatherListFragment newInstance(int columnCount) {
//        WeatherListFragment fragment = new WeatherListFragment();
//        Bundle args = new Bundle();
//        args.putInt(ARG_COLUMN_COUNT, columnCount);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (getArguments() != null) {
//            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        recyclerView = view.findViewById(R.id.list);

        getWeather();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(WeatherSummary item);
    }

    private void getWeather() {
        ApiService apiService = ApiClient.getClient().create(ApiService.class);

        Call<ResultResp> resultRespCall = apiService.WeatherList();

        resultRespCall.enqueue(new Callback<ResultResp>() {
            @Override
            public void onResponse(Call<ResultResp> call, Response<ResultResp> response) {
                if(response.code() == 200) {
                    List<ListWeather> w = response.body().getList();

                    WeatherSummary weatherSummary;

                    List<WeatherSummary> weatherSummaries =  new ArrayList<WeatherSummary>();

                    for(int i=0; i<w.size(); i++) {
                        weatherSummary = new WeatherSummary();

                        weatherSummary.setCountryCode(w.get(i).getSys().getCountry());

                        weatherSummary.setCountryID(w.get(i).getId());
                        weatherSummary.setLocName(w.get(i).getName());
                        weatherSummary.setWeatherDesc(w.get(i).getWeather().get(0).getDescription());
                        weatherSummary.setWeatherTitle(w.get(i).getWeather().get(0).getMain());
                        weatherSummary.setWeatherIcon("https://openweathermap.org/img/w/"+w.get(i).getWeather().get(0).getIcon()+".png");
                        weatherSummary.setTemp(w.get(i).getMain().getTemp());

                        weatherSummaries.add(weatherSummary);


                    }

                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerView.setLayoutManager(llm);
                    recyclerView.setAdapter(new MyWeatherListRecyclerViewAdapter(weatherSummaries, mListener));

                }
            }

            @Override
            public void onFailure(Call<ResultResp> call, Throwable t) {
                Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
