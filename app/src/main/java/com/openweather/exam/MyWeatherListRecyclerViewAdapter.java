package com.openweather.exam;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.openweather.exam.WeatherListFragment.OnListFragmentInteractionListener;
import com.openweather.exam.entity.WeatherSummary;

import java.util.List;


public class MyWeatherListRecyclerViewAdapter extends RecyclerView.Adapter<MyWeatherListRecyclerViewAdapter.ViewHolder> {

    private final List<WeatherSummary> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyWeatherListRecyclerViewAdapter(List<WeatherSummary> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.txtLocation.setText(mValues.get(position).getLocName());
        holder.txtWeather.setText(mValues.get(position).getWeatherTitle());
        holder.txtTemp.setText(String.valueOf(mValues.get(position).getTemp()));

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        public final TextView txtLocation;
        public final TextView txtWeather;
        public final TextView txtTemp;

        public WeatherSummary mItem;

        public ViewHolder(View view) {
            super(view);

            mView = view;
            txtLocation = view.findViewById(R.id.txtLocation);
            txtWeather = view.findViewById(R.id.txtWeather);
            txtTemp = view.findViewById(R.id.txtTemp);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + txtLocation.getText() + "'";
        }
    }
}
