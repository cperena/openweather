package com.openweather.exam;

import com.openweather.exam.responses.Coord;
import com.openweather.exam.responses.Result;
import com.openweather.exam.responses.ResultResp;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiService {
    @GET("/data/2.5/group?id=2643743,5391959,3067696&units=metric&appid=72535be0604fee283e9f63598c7b4836")
    Call<ResultResp> WeatherList();

    @GET("/data/2.5/weather")
    Call<Result> getWeatherInfo( @QueryMap Map<String, String> options
    );
}
