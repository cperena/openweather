package com.openweather.exam.entity;

public class WeatherSummary {

    private int CountryID;

    private String CountryCode;
    private String LocName;
    private String WeatherTitle;
    private String WeatherDesc;
    private Double temp;
    private String WeatherIcon;

    public int getCountryID() {
        return CountryID;
    }

    public void setCountryID(int countryID) {
        CountryID = countryID;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getLocName() {
        return LocName;
    }

    public void setLocName(String locName) {
        LocName = locName;
    }

    public String getWeatherTitle() {
        return WeatherTitle;
    }

    public void setWeatherTitle(String weatherTitle) {
        WeatherTitle = weatherTitle;
    }

    public String getWeatherDesc() {
        return WeatherDesc;
    }

    public void setWeatherDesc(String weatherDesc) {
        WeatherDesc = weatherDesc;
    }

    public String getWeatherIcon() {
        return WeatherIcon;
    }

    public void setWeatherIcon(String weatherIcon) {
        WeatherIcon = weatherIcon;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }
}
