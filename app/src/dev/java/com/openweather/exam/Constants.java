package com.openweather.exam;

public class Constants {
    public enum AppType {
        DEV, PROD, TEST
    }

    public static final AppType type = AppType.DEV;
}
